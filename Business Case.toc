\contentsline {paragraph}{To propose the replacement of American Academy Of Orthopaedic Surgeon\IeC {\textquoteright }s intranet platform, and explore the benefits and impact of doing so. By organizing our information assets that are spread throughout our organization via Shared Drive, the current intranet site, and the multiple SharePoint site collections into and on one platform that enables real-time collaboration and information sharing, thus improving communication from top to bottom.}{1}
\contentsline {section}{\numberline {1}Executive Summary}{4}
\contentsline {subsection}{\numberline {1.1}Current Situation and Major Issues / Pain-points}{4}
\contentsline {subsection}{\numberline {1.2}Potential Solution}{4}
\contentsline {subsection}{\numberline {1.3}Financial Summary}{5}
\contentsline {section}{\numberline {2}Background}{6}
\contentsline {subsection}{\numberline {2.1}Business Objectives}{6}
\contentsline {subsection}{\numberline {2.2}Business Drivers / Goals}{6}
\contentsline {subsection}{\numberline {2.3}Objectives}{6}
\contentsline {subsection}{\numberline {2.4}Strategic Alignment}{6}
\contentsline {subsection}{\numberline {2.5}Current Situation}{6}
\contentsline {subsection}{\numberline {2.6}Problem / Pain-points}{6}
\contentsline {subsection}{\numberline {2.7}Future Implications}{6}
\contentsline {section}{\numberline {3}Proposed Solution}{7}
\contentsline {subsection}{\numberline {3.1}Method of Solution Selection}{7}
\contentsline {subsection}{\numberline {3.2}Potential Solutions}{7}
\contentsline {subsection}{\numberline {3.3}Preferred Solution}{7}
\contentsline {subsection}{\numberline {3.4}Other Vendors}{7}
\contentsline {subsection}{\numberline {3.5}Expected Business Impact}{7}
\contentsline {subsection}{\numberline {3.6}Business Readiness and Risks}{7}
\contentsline {section}{\numberline {4}Financial Analysis}{8}
\contentsline {subsection}{\numberline {4.1}Budgetary Context}{8}
\contentsline {subsection}{\numberline {4.2}Solution Costs}{8}
\contentsline {subsection}{\numberline {4.3}Software Costs}{8}
\contentsline {subsection}{\numberline {4.4}Professional Service}{8}
\contentsline {subsection}{\numberline {4.5}Hardware}{8}
\contentsline {subsection}{\numberline {4.6}On-going Costs}{8}
\contentsline {subsection}{\numberline {4.7}Internal Costs}{8}
\contentsline {subsection}{\numberline {4.8}Total Cost of Ownership over 3 years}{8}
\contentsline {section}{\numberline {5}Productivity Gains}{9}
\contentsline {subsection}{\numberline {5.1}People Directory}{9}
\contentsline {subsection}{\numberline {5.2}Search}{9}
\contentsline {subsection}{\numberline {5.3}Version Control}{9}
\contentsline {subsection}{\numberline {5.4}Electronic Forms}{9}
\contentsline {subsection}{\numberline {5.5}ROI from Project Goals}{9}
\contentsline {section}{\numberline {6}Next Steps}{10}
\contentsline {subsection}{\numberline {6.1}Project Timescales}{10}
\contentsline {subsection}{\numberline {6.2}Steering Group / Project Team}{10}
\contentsline {subsection}{\numberline {6.3}Roles and Responsibilities}{10}
\contentsline {section}{\numberline {7}The benefits for the departments}{11}
\contentsline {subsection}{\numberline {7.1}Technology and Data Services}{11}
\contentsline {subsection}{\numberline {7.2}Human Resources Department}{11}
\contentsline {subsection}{\numberline {7.3}Executive and Management}{11}
\contentsline {subsection}{\numberline {7.4}Finance and Accounting}{11}
\contentsline {section}{\numberline {8}Business Case Approval}{12}
