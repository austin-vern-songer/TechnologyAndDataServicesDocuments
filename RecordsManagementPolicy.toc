\contentsline {section}{\numberline {1}Policy}{2}
\contentsline {paragraph}{It is the policy of the American Academy of Orthopaedic Surgeons to accommodate the timely storage, retrieval, and disposition of records created, utilized, and maintained by the various departments. The period of time that records are maintained is based on the minimum requirements set forth in state and federal retention schedules.}{2}
\contentsline {section}{\numberline {2}Role of Retention Center}{2}
\contentsline {paragraph}{The role of the Retention Center is to receive, maintain, destroy, and service inactive records that have not met their disposition date. Each business unit is to establish schedules to comply with the minimum amount of time records should be maintained in compliance with state and federal guidelines. Retention requirements apply whether or not the records are transferred to the Retention Center. Copies of the schedules must be maintained by the business unit and available for inspection.}{2}
\contentsline {section}{\numberline {3}Role of Records Manager}{2}
\contentsline {paragraph}{The role of the Records Manager is to administer the Records Management program. The Records Manager is well acquainted with all records and record groups within an agency and has expertise in all aspects of records management. The duties of the Records Manager include planning, development, and administration of records management policies. These duties also include the annual organizationwide inventory of all information assets to be conducted by the business unit manager with reports sent to the Records Manager.}{2}
\contentsline {section}{\numberline {4}Role of Management Personnel}{2}
\contentsline {section}{\numberline {5}Role of Departmental Records Coordinator}{2}
\contentsline {section}{\numberline {6}Type of Documents Maintained In Retention Center}{2}
\contentsline {section}{\numberline {7}Services}{2}
\contentsline {section}{\numberline {8}Transferring Records}{2}
\contentsline {section}{\numberline {9}Record Retrieval}{2}
\contentsline {section}{\numberline {10}Record Destruction}{2}
