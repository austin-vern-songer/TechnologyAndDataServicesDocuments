# American Academy of Orthopaedic Surgeons
## Technology and Data Services


  
| **Documents InProgress**  | **Documents Completed** |
|--------------|--------------|
|[Disaster Recovery Plan Manual](https://gitlab.com/austinvernsonger1/TechnologyAndDataServicesDocuments/blob/master/Disaster%20Recovery%20Plan%20Manual.tex "Disaster Recovery Plan Manual")  |  |
|[Records Management Manual](https://gitlab.com/austinvernsonger1/TechnologyAndDataServicesDocuments/blob/master/Records%20Management%20Manual.tex "Records Management Manual")|  |
|[Conflict of Interest](https://gitlab.com/austinvernsonger1/TechnologyAndDataServicesDocuments/blob/master/Conflict%20of%20Interest.tex "Conflict of Interest")  |  |
|[Information Security Standards Manual](https://gitlab.com/austinvernsonger1/TechnologyAndDataServicesDocuments/blob/master/Information%20Security%20Standards%20Manual.Tex "Information Security Standards Manual")  |  |
|  |  |
|  |  |
|  |  |


> 

| **Current Projects**  |
|--------------|
|  |  
|  |  
|  |  
|  |  
 
> 

| **Stardard Work**  |
|--------------|
|  |  
|  |  
|  |  
|  | 

> 

| **Standard Operating Procedures**  |
|--------------|
|  |  
|  |  
|  |  
|  |  

>
