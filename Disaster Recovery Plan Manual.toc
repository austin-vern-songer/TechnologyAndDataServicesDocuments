\contentsline {section}{\numberline {1}Introduction}{3}
\contentsline {section}{\numberline {2}Definition of a Disaster }{3}
\contentsline {section}{\numberline {3}Purpose}{3}
\contentsline {section}{\numberline {4}Scope}{4}
\contentsline {section}{\numberline {5}Version Information & Changes }{5}
\contentsline {section}{\numberline {6}Disaster Recovery Teams & Responsibilities}{5}
\contentsline {section}{\numberline {7}Disaster Recovery Lead}{5}
\contentsline {subsection}{\numberline {7.1}Role and Responsibilities}{6}
\contentsline {subsection}{\numberline {7.2}Contact Information}{6}
\contentsline {section}{\numberline {8}Disaster Management Team}{6}
\contentsline {subsection}{\numberline {8.1}Role & Responsibilities}{6}
\contentsline {subsection}{\numberline {8.2}Contact Information}{7}
\contentsline {section}{\numberline {9}Facilities Team}{8}
\contentsline {section}{\numberline {10}Network Team}{8}
\contentsline {section}{\numberline {11}Server Team}{8}
\contentsline {section}{\numberline {12}Applications Team}{8}
\contentsline {section}{\numberline {13}Operations Team}{8}
\contentsline {section}{\numberline {14}Senior Management Team}{8}
\contentsline {section}{\numberline {15}Communication Team}{8}
\contentsline {section}{\numberline {16}Finance Team}{8}
\contentsline {section}{\numberline {17}Other Organization Specific Teams}{8}
\contentsline {section}{\numberline {18}Disaster Recovery Call Tree}{8}
\contentsline {section}{\numberline {19}Recovery Facilities}{8}
\contentsline {section}{\numberline {20}Description of Recovery Facilities}{8}
\contentsline {section}{\numberline {21}Transportation to the Standby Facility}{8}
\contentsline {section}{\numberline {22}Operational Considerations }{8}
\contentsline {section}{\numberline {23}Data and Backups}{8}
\contentsline {section}{\numberline {24}Communicating During a Disaster}{8}
\contentsline {section}{\numberline {25}Communicating with the Authorities}{8}
\contentsline {section}{\numberline {26}Communicating with Employees}{8}
\contentsline {section}{\numberline {27}Communicating with Clients}{8}
\contentsline {section}{\numberline {28}Communicating with Vendors}{8}
\contentsline {section}{\numberline {29}Communicating with the Media}{8}
\contentsline {section}{\numberline {30}Communicating the Disaster}{9}
\contentsline {section}{\numberline {31}Dealing with a Disaster}{10}
\contentsline {section}{\numberline {32}Disaster Identification and Declaration}{11}
\contentsline {section}{\numberline {33}DRP Activation}{12}
\contentsline {section}{\numberline {34}Assessment of Current and Prevention of Further Damage}{13}
\contentsline {section}{\numberline {35}Standby Facility Activation}{14}
\contentsline {section}{\numberline {36}Restoring IT Functionality }{15}
\contentsline {section}{\numberline {37}Repair & Rebuilding of Primary Facility}{16}
\contentsline {section}{\numberline {38}Other Organization Specific Steps Required}{17}
\contentsline {section}{\numberline {39}Restoring IT Functionality}{18}
\contentsline {section}{\numberline {40}Current System Architecture}{19}
\contentsline {section}{\numberline {41}IT Systems}{20}
\contentsline {section}{\numberline {42}Plan Testing & Maintenance }{21}
\contentsline {section}{\numberline {43}Maintenance}{22}
\contentsline {section}{\numberline {44}Testing}{23}
\contentsline {section}{\numberline {45}Call Tree Testing }{24}
\contentsline {paragraph}{Call Tree testing of the call trees is normally a good idea. Feel free to omit this section if you feel that it is irrelevant.}{24}
