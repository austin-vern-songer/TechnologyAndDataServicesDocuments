## Project planning, scheduling, and budgeting
A. Project kickoff
B. Establish project sponsor
C. Identify benefits and costs
D. Develop business case
E. Establish objectives
F. Define project scope
G. Define project approach
H. Define project activities
I. Develop project schedule
J. Prepare project budget
K. Determine project staffing requirements
L. Establish roles and responsibilities
M. Conduct project status assessment

Training
A. Determine training requirements
B. Identify and acquire tools
C. Develop training plan
D. Manage training activities
E. Establish budget status reporting methods
F. Establish schedule status reporting methods
G. Conduct project status assessment
III. Project control
A. Monitor project progress
B. Identify and resolve issues
C. Manage exception situations
D. Review and revise project plan
E. Conduct project status assessment
IV. Project quality procedures
A. Review enterprise documentation standards
B. Define quality objectives
C. Define product quality control reviews
D. Define documentation standards for policies
E. Define documentation standards for procedures
F. Develop quality plan
G. Define policy/procedure review strategies
H. Define documentation management plan
I. Identify/define support tools and procedures
J. Conduct project status assessment
V. Develop policies
A. Document definitions
B. Identify required policies
C. Identify procedures, standards required
D. Determine formatting
E. Outline content
F. Develop and define policies

G. Develop and define standards
H. Develop and define guidelines
I. Develop and define procedures
J. Conduct project status assessment
VI. Communications planning
A. Identify audiences
B. Determine distribution frequency requirements
C. Determine information distribution mechanisms
D. Develop communications plan
E. Define performance reporting requirements
F. Conduct project status assessment
VII. Project closure
A. Complete final evaluations
B. Initiate maintenance process
C. Close outstanding project work
D. Collect project feedback
E. Compile project closure documents

